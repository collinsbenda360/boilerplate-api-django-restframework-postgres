from django.shortcuts import render

# from rest_framework.decorators import api_view
# from rest_framework.response import Response
# from rest_framework.reverse import reverse


def index(request):
    context = {"message": "YiP!"}
    return render(request, "index.html", context)
