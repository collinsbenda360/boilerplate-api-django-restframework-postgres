import React, { useContext } from 'react';
import { Helmet } from 'react-helmet';
import { createContext } from 'react';

import { IReactChildren } from 'src/types/react-children';
import { useLocation } from 'react-router-dom';
import { appMeta } from 'src/config';

interface IDocumentContextProps {
  pageTitle: string;
}

const DocumentContext = createContext({} as IDocumentContextProps);

export function DocumentProvider({ children }: IReactChildren) {
  const [pageTitle, setPageTitle] = React.useState('');

  const location = useLocation();
  const { landing } = appMeta;

  React.useEffect(() => {
    const updateMeta = () => {
      switch (location.pathname) {
        case '/':
          setPageTitle(landing.title);
          break;
        case '/login':
          setPageTitle('Login');
          break;
        case '/signup':
          setPageTitle('Create Account');
          break;
        case '/dashboard':
          setPageTitle('Dashboard');
          break;
        case '/todos':
          setPageTitle('Todos');
          break;
        default:
          break;
      }
    };
    updateMeta();
  }, [location]);

  const { title } = appMeta;

  return (
    <DocumentContext.Provider value={{ pageTitle }}>
      <Helmet>
        <title>
          {pageTitle} | {title}
        </title>
        <meta name="description" content={appMeta.description} />
      </Helmet>
      {children}
    </DocumentContext.Provider>
  );
}

export function useDocument() {
  return useContext(DocumentContext);
}

export { DocumentContext };
