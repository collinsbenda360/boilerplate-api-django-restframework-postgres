import os

# from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from user.models import User


class Command(BaseCommand):
    """
    Creates a super user if not exists.
    Todo: use env variables and strong password
    """

    def handle(self, *args, **options):
        email = os.environ.get("API_SUPERUSER_USERNAME")
        password = os.environ.get("API_SUPERUSER_PASSWORD")
        if User.objects.count() == 0:

            print("Creating account for %s (%s)" % (email, password))
            admin = User.objects.create_superuser(
                email=email, username=email, password=password
            )
            admin.is_active = True
            admin.is_admin = True
            admin.save()
        else:
            print("Admin accounts can only be initialized if no Accounts exist")
