# Generated by Django 3.2.2 on 2021-05-15 19:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0010_auto_20210510_0021'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='recover_account_token',
            field=models.CharField(default=None, max_length=90, null=True, unique=True),
        ),
        migrations.AddField(
            model_name='user',
            name='recover_account_token_expires',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
