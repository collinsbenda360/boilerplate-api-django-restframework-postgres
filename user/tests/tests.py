# import time

from user.models import User
from rest_framework import status
from rest_framework.test import APIClient, APITestCase


class UserAuthTest(APITestCase):
    """
    Test module for GET a todo API
    """

    def setUp(self):
        """
        Test setup
        """

        # Create the client to make requests
        self.client = APIClient()

        # create a test user that was just created
        # but has not had their email verified yet
        self.new_user_email_not_confirmed = User(
            username="testuser1@test.com",
            email="testuser1@test.com",
            email_verified=False,
        )

        self.new_user_email_not_confirmed.set_password("password")
        self.new_user_email_not_confirmed.save()

        # create a test user that has a verified email
        self.new_user_email_verified = User(
            username="testuser2@test.com",
            email="testuser2@test.com",
            email_verified=True,
        )

        self.new_user_email_verified.set_password("password")
        self.new_user_email_verified.save()

    def test_user_register(self):
        """
        Test user registration.
        """

        # ##################################
        # Create a new user
        #
        # TODO: test password requirements
        # ##################################

        creds = {
            "email": "test@test.com",
            "password": "123456789",
            "confirm_password": "123456789",
        }

        url = "/api/v1/user/register"

        # create new user
        response = self.client.post(url, creds, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        new_user = User.objects.filter(email=creds["email"]).first()

        # test new user defaults
        self.assertEqual(new_user.email, creds["email"])
        self.assertEqual(new_user.email_verified, False)
        self.assertEqual(new_user.is_active, True)
        self.assertNotEqual(new_user.registration_token, None)

        # ##################################
        # Test duplicate user registration
        #
        # ##################################

        response = self.client.post(url, creds, format="json")

        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        self.assertEqual(
            response.data["email"],
            ["Email address is already in use. Try to log in instead."],
        )

    def test_user_login_before_account_confirm(self):
        """
        Test user login attempt before account confirm
        """

        creds = {
            "email": "testuser1@test.com",
            "password": "password",
        }

        url = "/api/v1/user/login"

        response = self.client.post(url, creds, format="json")

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, "Email not verified.")

    def test_user_confirm_account(self):
        """
        Test user confirm account
        """

        creds = {
            "email": "test@test.com",
            "password": "123456789",
            "confirm_password": "123456789",
        }

        url = "/api/v1/user/register"

        # create new user
        response = self.client.post(url, creds, format="json")

        # get newly created user
        user = User.objects.filter(email="test@test.com").first()

        # get registration token assigned to user
        token = user.registration_token

        # ensure email is not verified yet
        self.assertEqual(user.email_verified, False)

        # user clicks confirm link with token
        url = f"/api/v1/user/register/confirm?token={token}"
        response = self.client.get(url)

        # get updated user
        user = User.objects.filter(email="test@test.com").first()

        # ensure confirmed user
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(user.is_active, True)
        self.assertEqual(user.registration_token, None)
        self.assertEqual(user.email_verified, True)

    def test_user_login_after_being_confirmed(self):
        """
        Test user login cases
        """

        # ##################################
        # Field validation
        #
        # ##################################

        # no email
        creds = {
            # "email": "testuser1@test.com",
            "password": "passwords",
        }

        url = "/api/v1/user/login"

        response = self.client.post(url, creds, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["email"], ["This field is required."])

        # no pw
        creds = {
            "email": "testuser1@test.com",
            # "password": "passwords",
        }

        url = "/api/v1/user/login"

        response = self.client.post(url, creds, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["password"], ["This field is required."])

        # ##################################
        # Invalid creds
        #
        # ##################################

        creds = {
            "email": "testuser2@test.com",
            "password": "passwords",
        }

        url = "/api/v1/user/login"

        response = self.client.post(url, creds, format="json")

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.data, "The information you provided does not match our records."
        )

        # ##################################
        # Non existing user
        #
        # ##################################

        creds = {
            "email": "dne@test.com",
            "password": "passwords",
        }

        url = "/api/v1/user/login"

        response = self.client.post(url, creds, format="json")

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # ##################################
        # success
        #
        # ##################################

        creds = {
            "email": "testuser2@test.com",
            "password": "password",
        }

        url = "/api/v1/user/login"

        response = self.client.post(url, creds, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_lockout_attempts(self):
        """
        Test user lockouts
        """

        # ##################################
        # First create a new user
        #
        # ##################################

        creds = {
            "email": "test@test.com",
            "password": "123456789",
            "confirm_password": "123456789",
        }

        url = "/api/v1/user/register"

        # create new user
        response = self.client.post(url, creds, format="json")

        # get newly created user
        user = User.objects.filter(email="test@test.com").first()

        # get registration token assigned to user
        token = user.registration_token

        # ensure email is not verified yet
        self.assertEqual(user.email_verified, False)

        # user clicks confirm link with token
        url = f"/api/v1/user/register/confirm?token={token}"
        response = self.client.get(url)

        # get updated user
        user = User.objects.filter(email="test@test.com").first()

        # ensure confirmed user
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(user.locked_out, False)
        self.assertEqual(user.locked_out_expires, None)
        self.assertEqual(user.login_attempts, 0)

        # ##################################
        # Now try to login and test lockout
        #
        # ##################################

        creds = {
            "email": "test@test.com",
            "password": "zzzzzzzzzzzzzzzz",
        }

        url = "/api/v1/user/login"

        # for x in range(2):
        #     print(f"login attempt: {x}")
        response = self.client.post(url, creds, format="json")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # get updated user
        user = User.objects.filter(email="test@test.com").first()

        self.assertEqual(user.locked_out, False)
        self.assertEqual(user.login_attempts, 1)

        response = self.client.post(url, creds, format="json")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # get updated user
        user = User.objects.filter(email="test@test.com").first()

        self.assertEqual(user.locked_out, False)
        self.assertEqual(user.login_attempts, 2)

        response = self.client.post(url, creds, format="json")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # get updated user
        user = User.objects.filter(email="test@test.com").first()

        self.assertEqual(user.locked_out, True)
        self.assertEqual(user.login_attempts, 3)

        # TODO: figure out how to test without 61s sleep
        # time.sleep(61)

        # response = self.client.post(url, creds, format="json")
        # self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # # get updated user
        # user = User.objects.filter(email="test@test.com").first()

        # self.assertEqual(user.locked_out, False)
        # self.assertEqual(user.login_attempts, 0)

    def test_user_reset_password(self):

        # ##################################
        # register and confirm new user
        #
        # ##################################

        creds = {
            "email": "test@test.com",
            "password": "123456789",
            "confirm_password": "123456789",
        }

        url = "/api/v1/user/register"

        # create new user
        response = self.client.post(url, creds, format="json")

        # get newly created user
        user = User.objects.filter(email="test@test.com").first()

        # get registration token assigned to user
        token = user.registration_token

        # ensure email is not verified yet
        self.assertEqual(user.email_verified, False)

        # user clicks confirm link with token
        url = f"/user/register/confirm?token={token}"
        response = self.client.get(url)

        # get updated user
        user = User.objects.filter(email="test@test.com").first()

        # ##################################
        # make reset request
        #
        # ##################################

        creds = {"email": "test@test.com"}

        url = "/api/v1/user/account/forgot-pw"

        # request to change pw
        response = self.client.post(url, creds, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # get updated user
        user = User.objects.filter(email="test@test.com").first()

        # get registration token assigned to user
        token = user.recover_account_token

        # test
        self.assertEqual(user.locked_out, True)
        self.assertNotEqual(user.recover_account_token, None)
        self.assertNotEqual(user.recover_account_token_expires, None)

        # ##################################
        # user clicks reset link in email
        #
        # ##################################

        url = f"/api/v1/user/account/confirm-forgot-pw?token={token}"
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # get updated user
        user = User.objects.filter(email="test@test.com").first()
        self.assertEqual(user.locked_out, True)

        # ##################################
        # user puts a new password
        #
        # ##################################

        creds = {
            "email": "test@test.com",
            "password": "NewHard2Gue$$",
            "confirm_password": "NewHard2Gue$$",
        }

        url = "/api/v1/user/account/update-pw"

        # request to change pw
        response = self.client.put(url, creds, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # get updated user
        user = User.objects.filter(email="test@test.com").first()
        self.assertEqual(user.locked_out, False)
        self.assertEqual(user.recover_account_token, None)
        self.assertEqual(user.recover_account_token_expires, None)
        self.assertEquals(user.check_password("NewHard2Gue$$"), True)
