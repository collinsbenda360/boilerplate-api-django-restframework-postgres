const path = require('path');
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: './_assets/js/index.js',
  output: {
    filename: 'index-bundle.js',
    path: path.resolve(__dirname, './_staticfiles'),
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: "_assets/img", to: "." },
      ],
    }),
  ],
};
